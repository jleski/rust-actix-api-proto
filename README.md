# Rust API Proto
Small prototype microservice backed by CockroachDB.

## Set up

Pre-requisites: Install or set up connection to Docker via 'docker' cli.

**1. Set up bridge network**

    docker network create -d bridge roachnet

**2. Start CockroachDB node**

    docker run -d \
    --name=roach1 \
    --hostname=roach1 \
    --net=roachnet \
    -p 26257:26257 -p 8080:8080  \
    -v "${PWD}/cockroach-data/roach1:/cockroach/cockroach-data"  \
    cockroachdb/cockroach:v2.1.6 start --insecure
   
**3. Test the connectivity and create database**

    docker exec -it roach1 ./cockroach sql --insecure
    # Welcome to the cockroach SQL interface.
    # All statements must be terminated by a semicolon.
    # To exit: CTRL + D.
    #
    # Server version: CockroachDB CCL v2.1.6 (x86_64-unknown-linux-gnu, built 2019/03/04 23:21:07, go1.10.7) (same version as client)
    # Cluster ID: aeff9415-310e-4529-805f-ef5982d0f870
    #
    # Enter \? for a brief introduction.
    #
    root@:26257/defaultdb> create database diesel_demo;
    CREATE DATABASE
    
    Time: 30.935205ms
    
    root@:26257/defaultdb> ^D

**4. Install Diesel cli**

    cargo install diesel_cli --no-default-features --features postgres
    
**5. Configure dotenv (.env) file**

If you are hosting the Docker container on some other hosts (such as Docker Machine) you need to add the correct IP:

    echo DATABASE_URL=postgres://root@172.16.110.135:26257/diesel_demo > .env
    
**6. Setup Diesel**

Comment out ./migrations initial setup **up.sql** and **down.sql** SQL queries and replace with something more trivial like 'SELECT 1;'

    $ diesel setup
    $ diesel migration generate licenses
    Creating migrations/2019-03-12-183535_licenses/up.sql
    Creating migrations/2019-03-12-183535_licenses/down.sql

Implement up.sql:

    CREATE TABLE licenses (
      id SERIAL PRIMARY KEY,
      license_type VARCHAR NOT NULL,
      valid_from DATE,
      valid_until DATE
    )
    
Implement down.sql:

    DROP TABLE licenses
    
Create the table:

    diesel migration run
    
Note: If you run into problems with Diesel not being compatible with CockroachDB, just create the table manually by executing to the CockroachDB container shell and running the up.sql contents.

