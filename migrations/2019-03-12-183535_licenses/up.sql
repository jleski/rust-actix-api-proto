-- Your SQL goes here
CREATE TABLE licenses (
  id SERIAL PRIMARY KEY,
  license_type VARCHAR NOT NULL,
  valid_from DATE,
  valid_until DATE
)