#[macro_use]
extern crate diesel;
extern crate dotenv;

pub mod schema;
pub mod models;

use diesel::prelude::*;
use dotenv::dotenv;
use std::env;

use self::models::{License, NewLicense};
use chrono::{NaiveDateTime};

pub fn establish_connection() -> PgConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

pub fn create_license<'a>(conn: &PgConnection, license_type: &'a i32, valid_from: &'a str, valid_until: &'a str) -> License {
    use crate::schema::licenses;

    let new_license = NewLicense {
        license_type: *license_type,
        valid_from: NaiveDateTime::parse_from_rfc2822(&valid_from),
        valid_until: NaiveDateTime::parse_from_rfc2822(&valid_until)
    };

    diesel::insert_into(licenses::table)
        .values(&new_license)
        .get_result(conn)
        .expect("Error saving new license")
}