use actix_web::{HttpRequest, Responder};

pub fn get_licenses(req: &HttpRequest) -> impl Responder {
    "get_licenses()"
}

pub fn get_license(req: &HttpRequest) -> impl Responder {
    let id = req.match_info().get("id").unwrap_or("World");
    format!("get_license({})", id)
}

/*
fn getUsers(req: &HttpRequest) -> impl Responder {
    let to = req.match_info().get("name").unwrap_or("World");
    "getUsers"
}
*/

