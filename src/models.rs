use chrono::{NaiveDateTime};
use crate::schema::licenses;

#[derive(Debug, Queryable)]
pub struct License {
    pub id: i32,
    pub license_type: i32,
    pub valid_from: NaiveDateTime,
    pub valid_until: NaiveDateTime,
}

impl License {
    fn is_valid() -> bool {
        true
    }
}

#[derive(Debug, Insertable)]
#[table_name="licenses"]
pub struct NewLicense {
    pub license_type: i32,
    pub valid_from: NaiveDateTime,
    pub valid_until: NaiveDateTime,
}