table! {
    licenses {
        id -> Int4,
        license_type -> Nullable<Integer>,
        valid_from -> Timestamp,
        valid_until -> Nullable<Timestamp>,
    }
}