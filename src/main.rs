extern crate actix_web;
extern crate chrono;
extern crate uuid;

use actix_web::{server, App};

mod handlers;

fn main() {
    server::new(|| {
        App::new()
            .resource("/licenses", |r| r.f(handlers::get_licenses))
            .resource("/license/{id}", |r| r.f(handlers::get_license))
    })
        .bind("0.0.0.0:8080")
        .expect("Can not bind to port 8080")
        .run();
}